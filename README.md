**Zadanie**

Masz dany graf nieskierowany. Będziemy usuwać po kolei po jednej krawędzi, po każdej usuniętej krawędzi chcemy wiedzieć ile jest spójnych składowych.

**Wejście**

Pierwsza linia zawiera trzy liczby całkowite n, m , q (1 <= n <= 10^5, 1<= m <= 2 * 10^5, 1 <= q <= m). Następne m linii zawiera po 2 liczby całkowite a, b (1 <= a,b <= n). Oznaczające, że wierzchołki a i b są połączone krawędzią. Następne q linii zawiera po jednej liczbie całkowitej c (1 <= c <= m) oznaczającej, że usuwamy krawędź o numerze c (numeracja zgodna z kolejnością na wejściu).

**Wyjście**

Dla każdej usuniętej krawędzi napisz, ile nadal jest spójnych składowych.

**Przykład**

Dla danych wejściowych

3 2 2

1 2

2 3

1

2

poprawną odpowiedzią jest

2

3

**Uwaga**

Graf nie ma wielokrotnych krawędzi.